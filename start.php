<?php
elgg_register_event_handler('init', 'system', 'meteo_init');

function meteo_init() {
   
    elgg_register_css("weather-icons","mod/jadeMeteo/views/default/jadeMeteo/weather-icons");
    elgg_load_css('weather-icons');
    elgg_register_css("widgetui", "mod/jadeMeteo/views/default/jadeMeteo/widgetui");
    elgg_load_css('widgetui');

    elgg_register_library('jade:Meteo',__DIR__.'/lib/function.php');
    
    elgg_register_widget_type([
        'id' => 'meteo',
        'name' => elgg_echo('jade:meteo:meteo'),
        'description' => 'Meteo plugin',
    ]);
    
   
}



