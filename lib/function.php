<?php

/**
 * Get the JSON icon code and return it with
 * the equivalent icon of the weather icon's pack
 * @return Icon svg icon 
 * @param $icon get the JSON icon code name 
 */
function getIcon($icon) {

    $hour = date('G');

    switch ($icon) {

        case "clear-day":

            $weatherIcons = "wi wi-day-sunny";
            break;

        case "clear-night":

            $weatherIcons = "wi wi-night-clear";
            break;

        case "rain":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-rain";
            } else {
                $weatherIcons = "wi wi-night-rain";
            }

            break;

        case "snow":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-snow";
            } else {
                $weatherIcons = "wi wi-night-snow";
            }
            break;

        case "sleet":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-sleet";
            } else {
                $weatherIcons = "wi wi-night-sleet";
            }
            break;

        case "wind":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-windy";
            } else {
                $weatherIcons = "wi-night-alt-cloudy-windy";
            }
            break;

        case "fog":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-fog";
            } else {
                $weatherIcons = "wi wi-night-fog";
            }

            break;

        case "cloudy":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-cloudy";
            } else {
                $weatherIcons = "wi wi-night-cloudy";
            }

            break;

        case "partly-cloudy-day":

            $weatherIcons = "wi wi-cloudy";
            break;

        case "partly-cloudy-night":

            $weatherIcons = "wi wi-night-alt-cloudy";
            break;

        case "hail":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-hail";
            } else {
                $weatherIcons = "wi wi-night-hail";
            }
            break;

        case "thunderstorm":

            if ($hour >= 7 && $hour <= 20) {
                $weatherIcons = "wi wi-day-thunderstorm";
            } else {
                $weatherIcons = "wi wi-night-thunderstorm";
            }
            break;

        case "tornado":

            $weatherIcons = "wi wi-tornado";
            break;

        default:

            $weatherIcons = "wi wi-na";
            break;
    }
    return $weatherIcons;
}

/**
 * get the wind speed from the API 
 * it return the equivalent wind strengh with the beaufort scale
 * @return $beaufortIcon svg beaufort scale icon 
 * @param  $windSpeed rounded value of the wind speed in Km/h 
 */
function getWindSpeed($windSpeed) {

    switch ($windSpeed) {

        case ($windSpeed == 0):

            $beaufortIcon = "wi wi-wind-beaufort-0";
            break;

        case ($windSpeed >= 1 && $windSpeed <= 5):

            $beaufortIcon = "wi wi-wind-beaufort-1";
            break;

        case ($windSpeed >= 6 && $windSpeed <= 11):

            $beaufortIcon = "wi wi-wind-beaufort-2";
            break;

        case ($windSpeed >= 12 && $windSpeed <= 19):

            $beaufortIcon = "wi wi-wind-beaufort-3";
            break;

        case ($windSpeed >= 20 && $windSpeed <= 28):

            $beaufortIcon = "wi wi-wind-beaufort-4";
            break;

        case ($windSpeed >= 29 && $windSpeed <= 38):

            $beaufortIcon = "wi wi-wind-beaufort-5";
            break;

        case ($windSpeed >= 39 && $windSpeed <= 49):

            $beaufortIcon = "wi wi-wind-beaufort-6";
            break;

        case ($windSpeed >= 50 && $windSpeed <= 61):

            $beaufortIcon = "wi wi-wind-beaufort-7";
            break;

        case ($windSpeed >= 62 && $windSpeed <= 74):

            $beaufortIcon = "wi wi-wind-beaufort-8";
            break;

        case ($windSpeed >= 75 && $windSpeed <= 88):

            $beaufortIcon = "wi wi-wind-beaufort-9";
            break;

        case ($windSpeed >= 89 && $windSpeed <= 102):

            $beaufortIcon = "wi wi-wind-beaufort-10";
            break;

        case ($windSpeed >= 103 && $windSpeed <= 117):

            $beaufortIcon = "wi wi-wind-beaufort-11";
            break;

        case ($windSpeed >= 118):

            $beaufortIcon = "wi wi-wind-beaufort-12";
            break;

        default:

            $beaufortIcon = "wi wi-na";
            break;
    }
    return $beaufortIcon;
}

/**
 * get the wind bearing from the API in degrees and convert it  
 * into an understandable value for a lambda person (N/E/S/W)
 * @return $windBearing converted value of the wind bearing into (N/E/S/W)...
 *  @param $windBearing wind bearing in degrees retrieved from the API
 */
function getWindBearing($windBearing) {

    switch ($windBearing) {

        case ($windBearing >= 337 && $windBearing <= 22):

            $windBearing = 'N';
            break;

        case ($windBearing >= 23 && $windBearing <= 66):

            $windBearing = 'NE';
            break;

        case ($windBearing >= 67 && $windBearing <= 111):

            $windBearing = 'E';
            break;

        case ($windBearing >= 112 && $windBearing <= 156):

            $windBearing = 'SE';
            break;

        case ($windBearing >= 157 && $windBearing <= 201):

            $windBearing = 'S';
            break;

        case ($windBearing >= 202 && $windBearing <= 246):

            $windBearing = 'SO';
            break;

        case ($windBearing >= 247 && $windBearing <= 291):

            $windBearing = 'O';
            break;

        case ($windBearing >= 292 && $windBearing <= 336):

            $windBearing = 'NO';
            break;

        default:

            $windBearing = "N/A";
            break;
    }
    return $windBearing;
    
}
/**
 * get sunrise timestamp and convert it to normal time
 * @param type $sunriseTime timestamp sunrise time
 * @return $formatedTime time in hours and minutes
 */
function getSunriseTime($sunriseTime){
    
    $formatedSunrise = date('H:i', $sunriseTime);
    
    return $formatedSunrise;   
}

/**
 * get sunset timestamp and convert it to normal time
 * @param type $sunsetTime timestamp sunrise time
 * @return $formatedSunset time in hours and minutes
 */
function getSunsetTime($sunsetTime){
    
    $formatedSunset = date('H:i', $sunsetTime);
    
    return $formatedSunset;   
}


