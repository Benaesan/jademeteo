# README #

### Jade Meteo ###

* Jade Meteo is a weather plugin for Elgg 2.3.3
* using the Dark Sky API weather forecast

### Features ###

* Precise weather condition 
* Hourly forecast
* Day & Night icon cycle
* Temperature 
* Beaufort Scale
* Wind speed 
* Geolocation
