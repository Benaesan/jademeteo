<?php

elgg_load_library('jade:Meteo');

$widget = $vars['entity'];
$apiKey = elgg_get_plugin_setting('apiKey', 'jadeMeteo');

//USER LANGUAGE
$user = elgg_get_logged_in_user_entity();

//GEOLOCATION 
//Retrieving address
$address = $widget->city;
//input correction
$prepAddr = (string) str_replace(' ', '+', $address);
//API request
$geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '');
//decoding JSON geolocation data
$output = json_decode($geocode);

//retriving latitude coords 
$latitude = $output->results[0]->geometry->location->lat;
//retrieving longitude coords
$longitude = $output->results[0]->geometry->location->lng;
//retrieving city name
$cityName = $output->results[0]->address_components[0]->long_name;

//WEATHER FORECAST
//requesting API
$request = 'https://api.darksky.net/forecast/' . $apiKey . '/' . $latitude . ',' . $longitude . '?units=ca&lang=' . $user->language . '';
//retrieving JSON forecast data
$json = file_get_contents($request);
//decoding JSON forecast data
$response = json_decode($json);

//retrieving rounded temperature data 
$temp = round($response->hourly->data[0]->temperature);
//retrieving summary
$summary = $response->hourly->data[0]->summary;
//retrieving rounded wind speed data
$windSpeed = round($response->hourly->data[0]->windSpeed);
//retrieving wind bearing
$windBearing = $response->hourly->data[0]->windBearing;
//retriving icon
$icon = $response->hourly->data[0]->icon;
// Celsius icon
$celsius = "wi wi-celsius";

//retrieving apparent temp
$feltTemp = $response->hourly->data[0]->apparentTemperature;
//retriving precip prob
$precipProbability = $response->hourly->data[0]->precipProbability;
//retrieving humidity
$humidity = round($response->hourly->data[0]->humidity);
//cloud cover 
$cloudCover = round($response->hourly->data[0]->cloudCover) * 100;
//retrieving pressure 
$pressure = round($response->hourly->data[0]->pressure);
//retrieving ozone 
$ozone = round($response->hourly->data[0]->ozone);
//retrieving uv index
$uvIndex = $response->hourly->data[0]->uvIndex;
//get sunrise time
$sunriseTime = $response->daily->data[0]->sunriseTime;
//get sunset time  
$sunsetTime = $response->daily->data[0]->sunsetTime;

$sunriseIcon = "wi wi-sunrise";

echo getSunriseTime($sunriseTime);

//WEATHER WIDGET UI

echo'

    <div class="wrapper">
        <div class="cityText">' . $cityName . '</div>
            <div>
                <span class= "weatherIcon ' . getIcon($icon) . '"></span>
                <span class= "tempText"> ' . $temp . '</span>
                <span class= "degrees ' . $celsius . '"></span>
            </div>
            <div>
                <p class= "summary">' . $summary . '</p>
                <p class= "wind">
                    <span> Vent : ' . $windSpeed . ' Km/h </span>
                    <span>(' . getWindBearing($windBearing) . ')</span>
                </p>  
            </div>  
            <div class="beaufortWidth">
                <span class= "beaufort ' . getWindSpeed($windSpeed) . '"></span>
            </div>
            <div>
                <span class= "sunrise ' . $sunriseIcon . '"></span>
                <span class= "sunrise ' . getSunriseTime($sunriseTime) . '"></span>
            </div>
    </div>';

