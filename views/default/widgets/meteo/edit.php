<div>
    
    
      <label><?php echo elgg_echo('jade:meteo:city')?>:</label>
    
      <?php
        //This is an instance of the ElggWidget class that represents our widget.
        $widget = $vars['entity'];

        // Give the user a plain text box to input a message
        echo elgg_view('input/text', array(
            'name' => 'params[city]',
            'value' => $widget->city,
            'class' => 'hello-input-text',
        ));
    ?>
</div>
